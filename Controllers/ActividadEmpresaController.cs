﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
namespace Controllers
{
    public class ActividadEmpresaController : AbstractController<ActividadEmpresa>
    {
        public override void AddEntity(ActividadEmpresa entity)
        {
            ActividadEmpresa actividadEmpresa = GetEntity(entity.IdActividadEmpresa);
            if (actividadEmpresa == null)
            {
                em.ActividadEmpresa.Add(entity);
                em.SaveChanges();
            }
            else
            {
                throw new ArgumentException("No se puede registrar la actividad");
            }
        }

        public override void DeleteEntity(object key)
        {
            throw new NotImplementedException();
        }

        public override List<ActividadEmpresa> GetEntities()
        {
            return em.ActividadEmpresa.ToList<ActividadEmpresa>();
        }

        public override ActividadEmpresa GetEntity(object key)
        {
            return em.ActividadEmpresa.Where(p => p.IdActividadEmpresa == (int)key).FirstOrDefault<ActividadEmpresa>();
        }

        public override void UpdateEntity(ActividadEmpresa entity)
        {
            throw new NotImplementedException();
        }
    }
}
