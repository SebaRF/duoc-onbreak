﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Controllers
{
    public class ContratoController : AbstractController<Contrato>
    {
        public override void AddEntity(Contrato entity)
        {
            Contrato contrato = GetEntity(entity.Numero);
            if (contrato == null)
            {
                em.Contrato.Add(entity);
                em.SaveChanges();
            }
            else
            {
                throw new ArgumentException("No se puede registrar al Cliente.");
            }
        }

        public override void DeleteEntity(object key)
        {
            throw new NotImplementedException();
        }

        public override List<Contrato> GetEntities()
        {
            throw new NotImplementedException();
        }

        public override Contrato GetEntity(object key)
        {
            return em.Contrato.Where(p => p.Numero == (String)key).FirstOrDefault<Contrato>();
        }

        public override void UpdateEntity(Contrato entity)
        {
            throw new NotImplementedException();
        }
    }
}
