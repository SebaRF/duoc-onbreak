﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
namespace Controllers
{
    public class TipoEmpresaController : AbstractController<TipoEmpresa>
    {
        public override void AddEntity(TipoEmpresa entity)
        {
            TipoEmpresa autor = GetEntity(entity.IdTipoEmpresa);
            if (autor == null)
            {
                em.TipoEmpresa.Add(entity);
                em.SaveChanges();
            }
            else
            {
                throw new ArgumentException("No se puede registrar el autor");
            }
        }

        public override void DeleteEntity(object key)
        {
            throw new NotImplementedException();
        }

        public override List<TipoEmpresa> GetEntities()
        {
            return em.TipoEmpresa.ToList<TipoEmpresa>();
        }
        public override TipoEmpresa GetEntity(object key)
        {
            return em.TipoEmpresa.Where(p => p.IdTipoEmpresa == (int)key).FirstOrDefault<TipoEmpresa>();
        }

        public override void UpdateEntity(TipoEmpresa entity)
        {
            throw new NotImplementedException();
        }
    }
}
