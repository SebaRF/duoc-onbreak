﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Controllers
{
    public class ClienteController : AbstractController<Cliente>
    {

        #region CRUD

        public override void AddEntity(Cliente entity)
        {
            Cliente cliente = GetEntity(entity.RutCliente);
            if (cliente == null)
            {
                em.Cliente.Add(entity);
                em.SaveChanges();
            }
            else
            {
                throw new ArgumentException("No se puede registrar al Cliente.");
            }
        }

        public override List<Cliente> GetEntities()
        {
            return em.Cliente.ToList<Cliente>();
        }

        public override Cliente GetEntity(object key)
        {
            return em.Cliente.Where(p => p.RutCliente == (String)key).FirstOrDefault<Cliente>();
        }

        public override void UpdateEntity(Cliente entity)
        {
            Cliente c = GetEntity(entity.RutCliente);
            if (c != null)
            {
                c.RutCliente = entity.RutCliente;
                c.RazonSocial = entity.RazonSocial;
                c.NombreContacto = entity.NombreContacto;
                c.MailContacto = entity.MailContacto;
                c.Direccion = entity.Direccion;
                c.Telefono = entity.Telefono;
                c.IdActividadEmpresa = entity.IdActividadEmpresa;
                c.IdTipoEmpresa = entity.IdTipoEmpresa;
                em.SaveChanges();
            }
            else
            {
                throw new ArgumentException("No se logró realizar los cambios");
            }
        }

        public override void DeleteEntity(object key)
        {
            Cliente cliente = GetEntity(key);
            if (cliente != null)
            {
                em.Cliente.Remove(cliente);
                em.SaveChanges();
            }
            else
            {
                throw new ArgumentException("No se puede eliminar al Cliente");
            }
        } 
        #endregion

        public List<Cliente> ToSearchByRut(String rutCliente)
        {
            List<Cliente> clientes = (
                                        from c in em.Cliente 
                                        where c.RutCliente.Contains(rutCliente) 
                                        select c
                                     ).ToList();
            return clientes;
        }

        public List<Cliente> ToSearchByIdActivity(int idActivity)
        {
            List<Cliente> clientes = (
                                        from c in em.Cliente
                                        where c.IdActividadEmpresa == idActivity
                                        select c
                                     ).ToList();
            return clientes;
        }

        public List<Cliente> ToSearchByIdType(int idType)
        {
            List<Cliente> clientes = (
                                        from c in em.Cliente
                                        where c.IdTipoEmpresa == idType
                                        select c
                                     ).ToList();
            return clientes;
        }

       

    }
}
