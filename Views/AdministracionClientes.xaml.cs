﻿using Controllers;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Views
{
    public partial class AdministracionClientes : Window
    {
        private const int V = 0;

        #region Instancia de Controladores.
        ClienteController clienteController = new ClienteController();
        TipoEmpresaController tipoEmpresaController = new TipoEmpresaController();
        ActividadEmpresaController actividadEmpresaController = new ActividadEmpresaController();
        #endregion

        #region Constructor de la Clase.
        public AdministracionClientes()
        {
            InitializeComponent();
            Init();
        }
        #endregion

        #region Buttoms Eventos Click.
        private void btnCreateCliente_Click(object sender, RoutedEventArgs e)
        {
            RegistrarCliente();
        }

        private void btnReadCliente_Click(object sender, RoutedEventArgs e)
        {
            BuscarCliente();
        }

        private void btnUpdateCliente_Click(object sender, RoutedEventArgs e)
        {
            ModificarCliente();
            
        }

        private void btnDeleteCliente_Click(object sender, RoutedEventArgs e)
        {
            EliminarCliente();
            
        }
        #endregion

        #region Implementación CRUD.
        private void RegistrarCliente()
        {
            try
            {
                String rutCliente = txtRutCliente.Text;
                String razonSocial = txtRazonSocial.Text;
                String nombreContacto = txtNombreContacto.Text;
                String mailContacto = txtMailContacto.Text;
                String direccion = txtDireccion.Text;
                String telefono = txtTelefono.Text;

                TipoEmpresa idTipoEmpresa = new TipoEmpresa()
                {
                    IdTipoEmpresa = int.Parse(cboTipoEmpresa.SelectedValue.ToString())
                };

                ActividadEmpresa idActividadEmpresa = new ActividadEmpresa()
                {
                    IdActividadEmpresa = int.Parse(cboActividadEmpresa.SelectedValue.ToString())
                };


                Cliente cliente = new Cliente()
                {
                    RutCliente = rutCliente,
                    RazonSocial = razonSocial,
                    NombreContacto = nombreContacto,
                    MailContacto = mailContacto,
                    Direccion = direccion,
                    Telefono = telefono,
                    IdActividadEmpresa = idActividadEmpresa.IdActividadEmpresa,
                    IdTipoEmpresa = idTipoEmpresa.IdTipoEmpresa
                };

                clienteController.AddEntity(cliente);
                MessageBox.Show("Agregado correctamente\n" + cliente.IdActividadEmpresa + " " + cliente.IdTipoEmpresa, "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                CargarEstructuraInicial();
            }
            catch (System.NullReferenceException ex)
            {
                MessageBox.Show(ex.Message + "\nContacte al administrador", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex )
            {
                MessageBox.Show("No es posible registrar al Cliente " + "\nContacte al administrador", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void BuscarCliente()
        {
            try
            {
                String rutCliente = txtRutCliente.Text;
                if(rutCliente.Trim().Length == 0)
                {
                    MessageBox.Show("Debes ingresar un rut válido", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtRutCliente.Focus();
                    return;
                }
                Cliente c = clienteController.GetEntity(rutCliente);
                if(c != null)
                {
                    txtRutCliente.Text = c.RutCliente;
                    txtRazonSocial.Text = c.RazonSocial;
                    txtNombreContacto.Text = c.NombreContacto;
                    txtMailContacto.Text = c.MailContacto;
                    txtDireccion.Text = c.Direccion;
                    txtTelefono.Text = c.Telefono;
                    cboTipoEmpresa.SelectedValue = c.IdTipoEmpresa;
                    cboActividadEmpresa.SelectedValue = c.IdActividadEmpresa;
                }
                else
                {
                    MessageBox.Show("La personas que buscas no existe en nuestros registros", "Mensajes", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nContacte al administrador", "Mensajes", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void ModificarCliente()
        {
            try
            {
                String rutCliente = txtRutCliente.Text;
                String razonSocial = txtRazonSocial.Text;
                String nombreContacto = txtNombreContacto.Text;
                String mailContacto = txtMailContacto.Text;
                String direccion = txtDireccion.Text;
                String telefono = txtTelefono.Text;

                int idTipoEmpresa = int.Parse(cboTipoEmpresa.SelectedValue.ToString());
                int idActividadEmpresa = int.Parse(cboActividadEmpresa.SelectedValue.ToString());

                Cliente c = new Cliente()
                {
                    RutCliente = rutCliente,
                    RazonSocial = razonSocial,
                    NombreContacto = nombreContacto,
                    MailContacto = mailContacto,
                    Direccion = direccion,
                    Telefono = telefono,
                    IdActividadEmpresa = idActividadEmpresa,
                    IdTipoEmpresa = idTipoEmpresa
                };

                MessageBox.Show("Modificado correctamente\n", "Mensajes", MessageBoxButton.OK, MessageBoxImage.Information);
                clienteController.UpdateEntity(c);
                CargarEstructuraInicial();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nContacte al administrador", "Mensajes", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void EliminarCliente()
        {
            try
            {
                String rutCliente = txtRutCliente.Text;
                if(rutCliente.Trim().Length == 0)
                {
                    MessageBox.Show("Debes ingresar un rut válido", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtRutCliente.Text = String.Empty;
                    txtRutCliente.Focus();
                    return;
                }
                
                MessageBoxResult respuesta = MessageBox.Show("Estas seguro que deseas eliminar a la persona indica\nEste proceso no se puede deshacer", "Mensajes", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(respuesta == MessageBoxResult.Yes)
                {
                    clienteController.DeleteEntity(rutCliente);
                    MessageBox.Show("Eliminado correctamente", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Information);
                    CargarEstructuraInicial();
                }
            }
            catch (DbUpdateException e)
            {
                SqlException s = e.InnerException.InnerException as SqlException;
                if (s != null)
                {
                    MessageBox.Show("Cliente tiene contrato registrado", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensaje", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        private void Init()
        {
            cboTipoEmpresa.ItemsSource = tipoEmpresaController.GetEntities();
            cboActividadEmpresa.ItemsSource = actividadEmpresaController.GetEntities();
            cboTipoEmpresa.SelectedIndex = V;
            cboActividadEmpresa.SelectedIndex = V;
        }

        private void CargarEstructuraInicial()
        {
            txtRutCliente.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtNombreContacto.Text = string.Empty;
            txtMailContacto.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            cboTipoEmpresa.SelectedIndex = V;
            cboActividadEmpresa.SelectedIndex = V;
            txtRutCliente.Focus();
        }

        private void btnRefreshView_Click(object sender, RoutedEventArgs e)
        {
            CargarEstructuraInicial();
        }

        private void btnListadoCliente_Click(object sender, RoutedEventArgs e)
        {
            ListadoClientes lc = new ListadoClientes(this.clienteController);
            lc.Show();
        }
    }
}

