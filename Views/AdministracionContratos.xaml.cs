﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Models;
using Controllers;
namespace Views
{
    public partial class AdministracionContratos : Window
    {
        ContratoController contratoController = new ContratoController();

        public AdministracionContratos()
        {
            InitializeComponent();
        }

        private void btnCreateContrato_Click(object sender, RoutedEventArgs e)
        {
            Contrato c = new Contrato();
            c.Numero = DateTime.Now.ToString("yyyyMMddhhmm");
            c.Creacion = DateTime.Parse(DateTime.Now.ToShortDateString());
            c.Termino = DateTime.Now; ;
            c.RutCliente = "20158799";
            c.IdModalidad = "CB001";
            c.IdTipoEvento = 10;
            c.FechaHoraInicio = DateTime.Now; 
            c.FechaHoraTermino = DateTime.Now; 
            c.Asistentes = 10;
            c.PersonalAdicional = 2;
            c.Realizado = true;
            c.ValorTotalContrato = 10;
            c.Observaciones = "N/A";
            contratoController.AddEntity(c);

            MessageBox.Show(
                                "Numero Contrato\n" + 
                                DateTime.Now.ToString("yyyyMMddhhmm") +
                                "\nFecha Ceación Contrato " + DateTime.Now,
                                "Información", 
                                MessageBoxButton.OK, 
                                MessageBoxImage.Information
                           );
        }
    }
}
