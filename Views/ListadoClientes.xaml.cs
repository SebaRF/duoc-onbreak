﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Controllers;
using Models;

namespace Views
{
    public partial class ListadoClientes : Window
    {
        #region Instancia de objetos
        private ClienteController clienteController;
        ActividadEmpresaController actividadEmpresaController = new ActividadEmpresaController();
        TipoEmpresaController tipoEmpresaController = new TipoEmpresaController();
        #endregion

        #region Constructores de la clase.
        public ListadoClientes()
        {
            InitializeComponent();
        }

        public ListadoClientes(ClienteController cc)
        {
            this.clienteController = cc;
            InitializeComponent();
            dtgTablaClientes.ItemsSource = clienteController.GetEntities();
            cboActividadEmpresa.ItemsSource = actividadEmpresaController.GetEntities();
            cboTipoEmpresa.ItemsSource = tipoEmpresaController.GetEntities();
        }
        #endregion

        #region Eventos de Controladores para filtro.
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            dtgTablaClientes.ItemsSource = null;
            dtgTablaClientes.ItemsSource = clienteController.GetEntities();
        }

        private void txtRutCliente_KeyUp(object sender, KeyEventArgs e)
        {
            dtgTablaClientes.ItemsSource = clienteController.ToSearchByRut(txtRutCliente.Text);
        }

        private void cboActividadEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //dtgTablaClientes.ItemsSource = clienteController.ToSearchByIdActivity(cboActividadEmpresa.SelectedIndex);
            int idActividadEmpresa = int.Parse(cboActividadEmpresa.SelectedValue.ToString());
            dtgTablaClientes.ItemsSource = clienteController.ToSearchByIdActivity(idActividadEmpresa);
        }

        private void cboTipoEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int idTipoEmpresa = int.Parse(cboTipoEmpresa.SelectedValue.ToString());
            dtgTablaClientes.ItemsSource = clienteController.ToSearchByIdType(idTipoEmpresa);
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((AdministracionClientes)Application.Current.MainWindow).txtRutCliente.Text = "SOME TEXT";
        }
    }
}
