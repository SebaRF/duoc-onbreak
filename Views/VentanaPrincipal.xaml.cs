﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Views
{
    public partial class VentanaPrincipal : Window
    {

        AdministracionClientes admClienteView = new AdministracionClientes();
        AdministracionContratos admContratosView = new AdministracionContratos();

        public VentanaPrincipal()
        {
            InitializeComponent();
        }

        private void mnuAdmCliente_Click(object sender, RoutedEventArgs e)
        {
            admClienteView.Show();
        }

        private void mnuAdmContrato_Click(object sender, RoutedEventArgs e)
        {
            admContratosView.Show();
        }

        private void mnuitemSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void mnuListadoClientes_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
